<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{__('Shop')}}</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Load the jQuery JS library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Custom JS script -->
    <script type="text/javascript">
        var loggedIn = {{ session()->has('username') ? 'true' : 'false' }};
        var setLoggedIn = function (newValue) {
            loggedIn = newValue;
            $('.secured').toggle(loggedIn);
        };

        $(document).ready(function () {

            /**
             * A function that takes a products array and renders it's html
             *
             * The products array must be in the form of
             * [{
             *     "title": "Product 1 title",
             *     "description": "Product 1 desc",
             *     "price": 1
             * },{
             *     "title": "Product 2 title",
             *     "description": "Product 2 desc",
             *     "price": 2
             * }]
             */
            function renderList(items) {
                let html = '';

                switch(window.location.hash.split('/')[0]) {
                    case '#order':
                        $.each(items, function (key, product) {
                            html += [
                                '<div class="product">',
                                    '<img class="productImage" src="/storage/images/' + product.image_name + '" alt="' + product.title + '">',
                                    '<div class="productInfo">',
                                        '<div class="productTitle">' + product.title + '</div>',
                                        '<div class="productDescription">' + (product.description ? product.description : '') + '</div>',
                                        '<div class="productPrice">' + product.price + '</div>',
                                    '</div>',
                                '</div>'
                            ].join('');
                        });
                        break;
                    case '#orders':
                        $.each(items, function (key, order) {
                            html += [
                                '<div class="orderContainer">',
                                    '<div>{{ __('Order ID') }}: ' + order.id + '</div><hr>',
                                    '<div>{{ __('Order Date') }}: ' + order.created_at + '</div><hr>',
                                    '<div>{{ __('Client Name') }}: ' + order.name + '</div><hr>',
                                    '<div>{{ __('Contact Details') }}: ' + order.contact + '</div><hr>',
                                    '<div>{{ __('Order Comments') }}: ' + (order.comments ? order.comments : '') + '</div><hr>',
                                    '<div>{{ __('Total') }}: ' + order.total + '</div><hr>',
                                    '<a href="#order/' + order.id + '">{{ __('Go to order') }}</a>',
                                '</div>'
                            ].join('');
                        });
                        break;
                    case '#products':
                        $.each(items, function (key, product) {
                            html += [
                                '<div class="product">',
                                    '<img class="productImage" src="/storage/images/' + product.image_name + '" alt="' + product.title + '">',
                                    '<div class="productInfo">',
                                        '<div class="productTitle">' + product.title + '</div>',
                                        '<div class="productDescription">' + (product.description ? product.description : '') + '</div>',
                                        '<div class="productPrice">' + product.price + '</div>',
                                    '</div>',
                                    '<div class="edit_delete">',
                                        '<a href="#product/' + product.id + '">{{ __('Edit') }}</a>',
                                        '<a href="#products/' + product.id + '">{{ __('Delete') }}</a>',
                                    '</div>',
                                '</div>'
                            ].join('');
                        });
                        break;
                    case '#cart':
                        $.each(items, function (key, product) {
                            html += [
                                '<div class="product">',
                                    '<img class="productImage" src="/storage/images/' + product.image_name + '" alt="' + product.title + '">',
                                    '<div class="productInfo">',
                                        '<div class="productTitle">' + product.title + '</div>',
                                        '<div class="productDescription">' + (product.description ? product.description : '') + '</div>',
                                        '<div class="productPrice">' + product.price + '</div>',
                                    '</div>',
                                    '<a href="#cart/' + product.id + '">{{ __("Remove") }}</a>',
                                    '<br>',
                                '</div>'
                            ].join('');
                        });
                        break;
                    default:
                        $.each(items, function (key, product) {
                            html += [
                                '<div class="product">',
                                    '<img class="productImage" src="/storage/images/' + product.image_name + '" alt="' + product.title + '">',
                                    '<div class="productInfo">',
                                        '<div class="productTitle">' + product.title + '</div>',
                                        '<div class="productDescription">' + (product.description ? product.description : '') + '</div>',
                                        '<div class="productPrice">' + product.price + '</div>',
                                    '</div>',
                                    '<a href="#/' + product.id + '">{{ __("Add") }}</a>',
                                    '<br>',
                                '</div>'
                            ].join('');
                        });
                        break;
                }
                return html;
            }

            /**
             * URL hash change handler
             */
            window.onhashchange = function () {
                // First hide all the pages and messages
                $('.page').hide();
                $('.success').html('');
                $('.error').html('');
                let allInput = $('input');

                // Separate hash from parameters
                var parts = window.location.hash.split('/');
                let hash = parts[0];
                let param = parts[1];

                switch(hash) {
                    case '#order':
                        if (loggedIn === true) {
                            $('.order').show();

                            // Request the order from the server
                            $.ajax('/order' + (param ? '?id=' + param : ''), {
                                dataType: 'json',
                                success: function (response) {
                                    if (response.success) {
                                        // Renders the order
                                        $('.order .orderInfo').html(
                                            [
                                                '<div class="orderContainer">',
                                                    '<div>{{ __('Order ID') }}: ' + response.order.id + '</div><hr>',
                                                    '<div>{{ __('Order Date') }}: ' + response.order.created_at + '</div><hr>',
                                                    '<div>{{ __('Client Name') }}: ' + response.order.name + '</div><hr>',
                                                    '<div>{{ __('Contact Details') }}: ' + response.order.contact + '</div><hr>',
                                                    '<div>{{ __('Order Comments') }}: ' + (response.order.comments ? response.order.comments : '') + '</div><hr>',
                                                    '<div>{{ __('Total') }}: ' + response.order.total + '</div>',
                                                '</div>'
                                            ].join('')
                                        );
                                        // Renders the products list
                                        $('.order .list').html(renderList(response.products));
                                    } else {
                                        // In case of error clear page and render the problem
                                        $('.order .list').html('');
                                        $('.order .orderInfo').html('');
                                        $('.global').html(response.global);
                                    }
                                }
                            });
                        }
                        break;
                    case '#orders':
                        if (loggedIn === true) {
                            $('.orders').show();

                            // Renders the orders list
                            $.ajax('/orders', {
                                dataType: 'json',
                                success: function (response) {
                                    $('.orders .list').html(renderList(response));
                                }
                            });
                        }
                        break;
                    case '#product':
                        if (loggedIn === true) {
                            $('.productPage').show();

                            // Clears auto-completion in case the target product has changed
                            allInput.val('');

                            // Gets auto-complete values if 'edit' button is pressed
                            if (param !== undefined) {
                                $.ajax('/product?id=' + param, {
                                    dataType: 'json',
                                    method: 'GET',
                                    success: function (response) {
                                        $('[name="title"]').val(response.title);
                                        $('[name="description"]').val(response.description);
                                        $('[name="price"]').val(response.price);
                                    }
                                });
                            }
                        }
                        break;
                    case '#products':
                        if (loggedIn === true) {
                            $('.products').show();

                            // Sends request for logout
                            if (param === 'logout') {
                                $.ajax('/products?logout', {
                                    success: function () {
                                        setLoggedIn(false);
                                        window.location.hash = '#';
                                    }
                                });
                            } else {
                                $.ajax('/products' + (param ? '?id=' + param : ''), {
                                    dataType: 'json',
                                    success: function (response) {
                                        // Render the products in the products list
                                        $('.products .list').html(renderList(response));
                                    }
                                });
                            }
                        }
                        break;
                    case '#login':
                        $('.login').show();

                        break;
                    case '#cart':
                        // Shows the cart page
                        $('.cart').show();
                        // Clears auto-completion in case the target product has changed
                        allInput.val('');

                        // Loads the cart products from the server
                        $.ajax('/cart' + (param ? '?id=' + param : ''), {
                            dataType: 'json',
                            success: function (response) {
                                // Render the products in the cart list
                                $('.cart .list').html(renderList(response));
                            }
                        });
                        break;
                    default:
                        // If all else fails, always default to index
                        // Shows the index page
                        $('.index').show();
                        // Loads the index products from the server
                        $.ajax('/' + (param ? '?id=' + param : ''), {
                            dataType: 'json',
                            success: function (response) {
                                // Render the products in the index list
                                $('.index .list').html(renderList(response));
                            }
                        });
                        break;
                }
            };

            window.onhashchange();

            // Sends request for product adding or editing
            $('#productForm').submit(function(event) {
                event.preventDefault();
                $('.error').html('');
                var param = window.location.hash.split('/')[1];

                $.ajax('/product' + (param === undefined ? '' : ('?id=' + param)), {
                    dataType: 'json',
                    method: 'POST',
                    data: new FormData(this),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    processData: false, // jQuery serializes data
                    contentType: false, // In this case, no conversion should take place because of file
                    success: function (response) {
                        if (response.success) {
                            window.location.hash = '#products';
                        } else {
                            $('.global').html(response.global);
                            $('.title').html(response.title);
                            $('.price').html(response.price);
                            $('.image').html(response.image);
                        }
                    }
                });
            });
            // Checks authentication
            $('#loginForm').submit(function (event) {
                event.preventDefault();
                $('.error').html('');

                $.ajax('/login', {
                    method: 'POST',
                    data: {
                        username: document.getElementsByName('username')[0].value,
                        passw: document.getElementsByName('passw')[0].value
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.success) {
                            setLoggedIn(true);
                            window.location.hash = '#';
                        } else {
                            $('.global').html(response.global);
                            $('.username').html(response.username);
                            $('.passw').html(response.passw);
                        }
                    }
                });
            });
            // Checks for form submit event and make the call
            $('#checkoutForm').submit(function(event) {
                event.preventDefault();
                $('.error').html('');

                $.ajax('/cart', {
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        name: document.getElementsByName('name')[0].value,
                        contact: document.getElementsByName('contact')[0].value,
                        comments: document.getElementsByName('comments')[0].value
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.success) {
                            $('.success').html(response.message);
                            $('.cart .list').html(renderList([]));
                        } else {
                            $('.global').html(response.global);
                            $('.name').html(response.name);
                            $('.contact').html(response.contact);
                        }
                    }
                });
            });
        });
    </script>
</head>
<body>
    <p class="success"></p>
    <p class="error global"></p>
    <!-- The index page -->
    <div class="page index">
        <a class="secured" href="#products" style="display: {{ session()->has('username') ? '' : 'none' }}">{{ __('Go to products') }}</a>

        <!-- The index element where the products list is rendered -->
        <div class="list"></div>

        <!-- A link to go to the cart by changing the hash -->
        <a href="#cart">{{__('Go to cart')}}</a>
        <a href="#login">{{__('Go to login')}}</a>
    </div>

    <!-- The cart page -->
    <div class="page cart">
        <!-- The cart element where the products list is rendered -->
        <div class="list"></div>

        <form method="POST" id="checkoutForm">

            <input type="text"
                   name="name"
                   placeholder="{{ __('Name') }}">
            <p class="error name"></p>

            <input type="text"
                   name="contact"
                   placeholder="{{ __('Contact details') }}">
            <p class="error contact"></p>

            <textarea name="comments" placeholder="{{ __('Comments') }}"></textarea>
            <br><br>

            <a href="#">{{ __('Go to index') }}</a>
            <button type="submit">{{ __('Checkout') }}</button>
        </form>
    </div>

    <!-- The login page -->
    <div class="page login">

        <form method="POST" id="loginForm">

            <input type="text" name="username" placeholder="{{ __('Username') }}">
            <p class="error username"></p>

            <input type="password" name="passw" placeholder="{{ __('Password') }}">
            <p class="error passw"></p>

            <button type="submit">{{ __('Login') }}</button>
        </form>
        <br>
        <a href="#">{{__('Go to index')}}</a>
    </div>

    <!-- The products page -->
    <div class="page products">

        <div class="list"></div>

        <a href="#product">{{ __('Add') }}</a>
        <a href="#products/logout">{{ __('Logout') }}</a>
        <a href="#">{{ __('Go to index') }}</a>
    </div>

    <!-- The product page -->
    <div class="page productPage">
        <form method="POST" enctype="multipart/form-data" id="productForm">

            <input type="text" name="title" placeholder="{{ __("Title") }}">
            <p class="error title"></p>

            <input type="text" name="description" placeholder="{{ __("Description") }}">
            <br><br>

            <input type="text" name="price" placeholder="{{ __("Price") }}">
            <p class="error price"></p>

            <input type="file" name="image" value="{{ __("Browse") }}">
            <p class="error image"></p>

            <a href="#products">{{ __("Products") }}</a>
            <button type="submit">{{ __("Save") }}</button>
        </form>
    </div>

    <!-- The orders page -->
    <div class="page orders">
        <div class="list"></div>
    </div>

    <!-- The order page -->
    <div class="page order">
        <div class="orderInfo"></div>
        <div class="list"></div>

        <a href="#orders">{{ __('Go to orders') }}</a>
    </div>

</body>
</html>

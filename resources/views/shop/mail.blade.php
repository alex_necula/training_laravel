<html lang='en'>
<head>
    <title>{{ __('New email') }}</title>
</head>
<body>
    <h1>{{ __('From') . ": " . $request->input('name') }}</h1>
    <h2>{{ __('Contact') . ": " . $request->input('contact') }}</h2>
    <h3>{{ __('Comments') . ": " . $request->input('comments') }}</h3>
    <hr>
    @foreach ($products as $product)
        <img width="50px" height='50px' alt="{{ $product->title }}" src="{{ url('/') }}/storage/images/{{ $product->image_name }}"><br>
        <p>{{ __('Title') . ": " . $product->title }}</p>
        <p>{{ __('Description') . ": " . $product->description }}</p>
        <p>{{ __('Price') . ": " . $product->price }}</p>
        <hr>
    @endforeach
</body>
</html>
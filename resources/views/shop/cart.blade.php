<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __('Shop') }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
    @if (!empty($products))
        <div id="productList">
            @foreach ($products as $product)
                <div class="product">
                    <img class="productImage" src="storage/images/{{ $product->image_name }}" alt="{{ $product->title }}">
                    <div class="productInfo">
                        <div class="productTitle">{{ $product->title }}</div>
                        <div class="productDescription">{{ $product->description }}</div>
                        <div class="productPrice">{{ $product->price }}</div>
                    </div>
                    <a href="/cart?id={{ $product->id }}">{{ __("Remove") }}</a>
                    <br>
                </div>
            @endforeach
        </div>
    @endif

    @if (session()->has('success'))
        <p class="success">{{ session('success') }}</p>
    @endif

    @foreach ($errors->get('global') as $error)
        <p class="error">{{ $error }}</p>
    @endforeach

    <form method="POST">
        @csrf

        <input type="text"
               name="name"
               value="{{ old('name') }}"
               placeholder="{{ __('Name') }}">
        @foreach ($errors->get('name') as $error)
            <br><br><span class="error">{{ $error }}</span>
        @endforeach
        <br><br>

        <input type="text"
               name="contact"
               value="{{ old('contact') }}"
               placeholder="{{ __('Contact details') }}">
        @foreach ($errors->get('contact') as $error)
            <br><br><span class="error">{{ $error }}</span>
        @endforeach
        <br><br>

        <textarea name="comments" placeholder="{{ __('Comments') }}">{{ old('comments') }}</textarea>
        <br><br>

        <a href="/">{{ __("Go to index") }}</a>
        <button type="submit">{{ __('Checkout') }}</button>
    </form>
</body>
</html>

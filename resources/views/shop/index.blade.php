<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __('Shop') }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
    @if (session()->has('username'))
        <a href="/products">{{ __("Go to products") }}</a>
    @endif

    @if (!empty($products))
        <div id="productList">
            @foreach ($products as $product)
                <div class="product">
                    <img class="productImage" src="storage/images/{{ $product->image_name }}" alt="{{ $product->title }}">
                    <div class="productInfo">
                        <div class="productTitle">{{ $product->title }}</div>
                        <div class="productDescription">{{ $product->description }}</div>
                        <div class="productPrice">{{ $product->price }}</div>
                    </div>
                    <a href="/index?id={{ $product->id }}">{{ __("Add") }}</a>
                    <br>
                </div>
            @endforeach
        </div>
    @endif

    <a href="/cart">{{ __("Go to cart") }}</a>
    <a href="/login">{{__('Go to login')}}</a>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __('Login') }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
    @foreach ($errors->get('global') as $error)
        <p class="error">{{ $error }}</p>
    @endforeach

    @if (!session()->has('username'))
        <form method="POST">
            @csrf

            <input type="text" name="username" placeholder="{{ __('Username') }}" value="{{ old('username') }}">
            @foreach ($errors->get('username') as $error)
                <br><br><span class="error">{{ $error }}</span>
            @endforeach
            <br><br>

            <input type="password" name="passw" placeholder="{{ __('Password') }}">
            @foreach ($errors->get('passw') as $error)
                <br><br><span class="error">{{ $error }}</span>
            @endforeach
            <br><br>

            <button type="submit">{{ __('Login') }}</button>
        </form>
    @endif
    <br>
    <a href="/">{{__('Go to index')}}</a>
</body>
</html>

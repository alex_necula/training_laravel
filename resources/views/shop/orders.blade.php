<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __("Orders") }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
    <body>
        @foreach ($orders as $order)
            <div class="orderContainer">
                <div>{{ __('Order ID') . ': ' . $order->id }}</div><hr>
                <div>{{ __('Order Date') . ': ' . $order->created_at }}</div><hr>
                <div>{{ __('Client Name') . ': ' . $order->name }}</div><hr>
                <div>{{ __('Contact Details') . ': ' . $order->contact }}</div><hr>
                <div>{{ __('Order Comments') . ': ' . $order->comments }}</div><hr>
                <div>{{ __('Total') . ': ' . $order->total() }}</div><hr>
                <a href="/order?id={{ $order->id }}">{{ __('Go to order') }}</a>
            </div>
        @endforeach
    </body>
</html>
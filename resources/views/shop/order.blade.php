<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __("Order details") }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
    @if (count($errors) > 0)
        @foreach ($errors->get('global') as $error)
            <p class="error">{{ $error }}</p>
        @endforeach
    @else
        <div class="orderContainer">
            <div>{{ __('Order ID') . ': ' . $order->id }}</div><hr>
            <div>{{ __('Order Date') . ': ' . $order->created_at }}</div><hr>
            <div>{{ __('Client Name') . ': ' . $order->name }}</div><hr>
            <div>{{ __('Contact Details') . ': ' . $order->contact }}</div><hr>
            <div>{{ __('Order Comments') . ': ' . $order->comments }}</div><hr>
            <div>{{ __('Total') . ': ' . $order->total() }}</div><hr>
            <a href="/order?id={{ $order->id }}">{{ __('Go to order') }}</a>
        </div>

        @foreach ($order->products as $product)
            <div class="product">
                <img class="productImage" src="/storage/images/{{ $product->image_name }}" alt="{{ $product->title }}">
                <div class="productInfo">
                    <div class="productTitle">{{ $product->title }}</div>
                    <div class="productDescription">{{ $product->description }}</div>
                    <div class="productPrice">{{ $product->price }}</div>
                </div>
            </div>
        @endforeach
    @endif

    <a href="/orders">{{ __('Go to orders') }}</a>
</body>
</html>
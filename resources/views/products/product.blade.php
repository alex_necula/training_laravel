<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __("Product Edit") }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    @foreach ($errors->get('global') as $error)
        <p class="error">{{ $error }}</p>
    @endforeach

    <form method="POST" enctype="multipart/form-data">
        @csrf

        <input type="text" name="title" placeholder="{{ __("Title") }}" value="{{ old('title') ?: $title }}">
        @foreach ($errors->get('title') as $error)
            <br><br><span class="error">{{ $error }}</span>
        @endforeach
        <br><br>

        <input type="text" name="description" placeholder="{{ __("Description") }}" value="{{ old('description') ?: $description }}">
        <br><br>

        <input type="text" name="price" placeholder="{{ __("Price") }}" value="{{ old('price') ?: $price }}">
        @foreach ($errors->get('price') as $error)
            <br><br><span class="error">{{ $error }}</span>
        @endforeach
        <br><br>

        <input type="file" name="image" value="{{ __("Browse") }}">
        @foreach ($errors->get('image') as $error)
            <br><br><span class="error">{{ $error }}</span>
        @endforeach
        <br><br>

        <a href="/products">{{ __("Products") }}</a>
        <button type="submit">{{ __("Save") }}</button>
    </form>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ __('Products') }}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
    @if (!empty($products))
        <div id="productList">
            @foreach ($products as $product)
                <div class="product">
                    <img class="productImage" src="storage/images/{{ $product->image_name }}" alt="{{ $product->title }}">
                    <div class="productInfo">
                        <div class="productTitle">{{ $product->title }}</div>
                        <div class="productDescription">{{ $product->description }}</div>
                        <div class="productPrice">{{ $product->price }}</div>
                    </div>
                    <div class="edit_delete">
                        <a href="/product?id={{ $product->id }}">{{ __('Edit') }}</a>
                        <a href="/products?id={{ $product->id }}">{{ __('Delete') }}</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

    <a href="/product">{{ __('Add') }}</a>
    <a href="/products?logout">{{ __('Logout') }}</a>
    <a href="/">{{ __('Go to index') }}</a>
</body>
</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('initCartList')->group(function () {
    Route::get('/', 'ShopController@index');
    Route::get('/index', 'ShopController@index');

    Route::get('/cart', 'ShopController@cart');
    Route::post('/cart', 'ShopController@checkout');

    Route::get('/spa', function () { return view('spa'); });
});

Route::get('/login', 'ShopController@login');
Route::post('/login', 'ShopController@auth');

Route::middleware('shopAuth')->group(function () {
    Route::get('/products', 'ProductsController@index');
    Route::get('/product', 'ProductsController@createOrEdit');
    Route::post('/product', 'ProductsController@store');

    Route::get('/orders', 'OrdersController@orders');
    Route::get('/order', 'OrdersController@order');
});

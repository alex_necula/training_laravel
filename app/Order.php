<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'contact', 'comments'];

    /**
     * Fetches products related to each order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Calculates the total price of products for each order
     *
     * @return mixed
     */
    public function total()
    {
        return $this->belongsToMany(Product::class)->sum('price');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Fetches orders related to each product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * Get products that are or aren't in the cart
     *
     * @param bool $cart
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public static function getProductsInOrNotInCart ($cart = false)
    {
        $productsIds = array_values(session('cartList'));
        $query = Product::query();
        $products = $cart ? $query->find($productsIds) : $query->whereNotIn('id', $productsIds)->get();

        return $products;
    }
}

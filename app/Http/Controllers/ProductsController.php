<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductForm;
use Illuminate\Support\Facades\Storage;
use App\Product;

class ProductsController extends Controller
{
    /**
     * View, delete and logout in products
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|array
     * @throws \Exception
     */
    public function index(Request $request)
    {
        // Logout and delete events
        if ($request->has('logout')) {
            session()->forget('username');
            return redirect('/login');
        } elseif ($request->filled('id') && ($product = Product::query()->find($request->query('id')))) {
            Storage::delete('/public/images/' . $product->image_name);
            $product->delete();
        }

        $products = Product::all();

        if ($request->ajax()) {
            return $products;
        }
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new product.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|array
     */
    public function createOrEdit(Request $request)
    {
        $title = $description = $price = '';
        if ($request->filled('id') && $product = Product::query()->find($request->query('id'))) {
            $title = $product->title;
            $description = $product->description;
            $price = $product->price;
        }

        if ($request->ajax()) {
            return [
                'title' => $title,
                'description' => $description,
                'price' => $price
            ];
        }

        return view('products.product')->with([
            'title' => $title,
            'description' => $description,
            'price' => $price
        ]);
    }

    /**
     * Updates or creates a product
     *
     * @param \App\Http\Requests\ProductForm $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|array
     */
    public function store(ProductForm $request)
    {
        $request->validated();

        // Update or create product
        $product = $request->has('id') ? Product::query()->find($request->query('id')) : new Product;

        // Upload new image and delete old image if needed
        if ($request->has('image')) {
            $fileName = $request->file('image')->store('/public/images');

            // Upload fails
            if ($fileName === false) {
                if ($request->ajax()) {
                    return ['success' => false, 'image' => __('Image could not be uploaded.')];
                }
                return redirect()->back()->withErrors('image', __('Image could not be uploaded.'));
            }

            // Retrieve only the file name from the path
            $fileName = explode('/', $fileName);
            $fileName = end($fileName);

            // Delete old image
            if ($request->has('id')) {
                Storage::delete('/public/images/' . $product->image_name);
            }

            $product->image_name = $fileName;
        }

        // Update/Insert to table
        $product->title = $request->input('title');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->save();

        if ($request->ajax()) {
            return ['success' => true];
        }
        return redirect('/products');
    }
}

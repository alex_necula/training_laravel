<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Order;

class OrdersController extends Controller
{
    /**
     * Orders page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|array
     */
    public function orders(Request $request)
    {
        $orders = Order::query()->orderBy('created_at', 'desc')->get();

        if ($request->ajax()) {
            $ordersWithTotal = [];
            foreach ($orders as $order) {
                $ordersWithTotal[] = array_merge(['total' => $order->total()], $order->toArray());
            }

            return $ordersWithTotal;
        }
        return view('shop.orders', compact('orders'));
    }

    /**
     * Specific order
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View|array
     */
    public function order(Request $request)
    {
        // ID was not passed correctly
        if (!$request->filled('id') || !is_numeric($request->query('id'))) {
            if ($request->ajax()) {
                return ['success' => false, 'global' => __('Order ID is invalid.')];
            }
            return redirect('/orders');
        }

        // ID is not in database
        if (($order = Order::query()->find($request->query('id'))) === null) {
            if ($request->ajax()) {
                return ['success' => false, 'global' => __('Order ID is invalid.')];
            }
            return view('shop.order')->withErrors(['global' => __('Order ID is invalid.')]);
        }

        if ($request->ajax()) {
            return ['success' => true, 'order' => array_merge(['total' => $order->total()], $order->toArray()), 'products' => $order->products];
        }

        return view('shop.order', compact('order'));
    }
}

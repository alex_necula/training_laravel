<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Http\Requests\CheckoutForm;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class ShopController extends Controller
{
    /**
     * Index route
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Model
     */
    public function index(Request $request)
    {
        // Add selected product to cart list
        if ($request->filled('id')
            && !in_array($request->query('id'), session('cartList'))
            && Product::query()->find($request->query('id'))) {

            session()->push('cartList', $request->query('id'));
        }

        $products = Product::getProductsInOrNotInCart();

        if ($request->ajax()) {
            return $products;
        }

        return view('shop.index', compact('products'));
    }

    /**
     * Handle cart route
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|Model
     */
    public function cart(Request $request)
    {
        // Remove selected product from cart
        if ($request->filled('id') && ($key = array_search($request->query('id'), session('cartList'))) !== false) {
            session()->forget('cartList.' . $key);
        }

        $products = Product::getProductsInOrNotInCart(true);

        if ($request->ajax()) {
            return $products;
        }

        return view('shop.cart', compact('products'));
    }

    /**
     * Checkout from cart
     *
     * @param \App\Http\Requests\CheckoutForm $request to be validated through 'CheckoutCart' form request
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function checkout(CheckoutForm $request)
    {
        $request->validated();

        $products = Product::getProductsInOrNotInCart(true);

        // Send the email with proper settings
        Mail::send('shop.mail', ['products' => $products, 'request' => $request], function ($message) {
            /** @var Message $message */
            $message->subject(__('New Command!'));
            $message->from(config('mail.from.address'));
            $message->to(config('mail.admin_address'));
        });

        // Check for email failures
        if (count(Mail::failures()) > 0) {
            if ($request->ajax()) {
                return ['success' => false, 'global' => __('Something went wrong, mail could not be sent.')];
            }

            return redirect()->back()->withErrors(['global' => __('Something went wrong, mail could not be sent.')]);
        }

        // Insert in database
        Order::query()->create([
            'name' => $request->input('name'),
            'contact' => $request->input('contact'),
            'comments' => $request->input('comments')
        ])->products()->attach($products);

        // Empty cart and redirect with success message
        session(['cartList' => []]);

        if ($request->ajax()) {
            return ['success' => true, 'message' => __('Mail successfully sent!')];
        }

        return redirect()->back()->with('success', __('Mail successfully sent!'));
    }

    /** @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View */
    public function login()
    {
        return view('shop.login');
    }

    /**
     * Authentication
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|array
     */
    public function auth(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'passw' => 'required'
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json($validator->errors(), 200);
            } else {
                return redirect('/login')
                    ->withErrors($validator);
            }
        }

        if ($request->input('username') === config('auth.admin.uname') && $request->input('passw') === config('auth.admin.pass')) {
            session(['username' => true]);
            if ($request->ajax()) {
                return ['success' => true];
            }
            return redirect('/');
        }

        if ($request->ajax()) {
            return ['success' => false, 'global' => __('Username and password invalid.')];
        }

        // Redirect in case of error
        return redirect()->back()->withErrors(['global' => __('Username and password invalid.')]);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class SessionInitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Initialize cart list
        if (!session()->has('cartList')) {
            session(['cartList' => []]);
        }
        return $next($request);
    }
}

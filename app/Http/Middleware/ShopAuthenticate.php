<?php

namespace App\Http\Middleware;

use Closure;

class ShopAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!session()->has('username')) {
            if ($request->ajax()) {
                return response('', 403);
            }
            return redirect('/');
        }
        return $next($request);
    }
}

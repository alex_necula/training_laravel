<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:63',
            'price' => 'required|numeric',
            'image' => 'sometimes|required|image|max:5120'
        ];
    }

    /** @param \Illuminate\Contracts\Validation\Validator $validator */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            /** @var \Illuminate\Contracts\Validation\Validator $validator */

            // URL has been changed
            if ($this->has('id') && \App\Product::query()->find($this->query('id')) === null) {
                $validator->errors()->add('global', __('Product does not exist in database.'));
            }

            // Trying to add a product with no image
            if (!$this->has('id') && !$this->has('image')) {
                $validator->errors()->add('image', __('The image field is required.'));
            }

            // Error uploading image
            if ($this->has('image') && $this->file('image')->isValid() === false) {
                $validator->errors()->add('image', __('There was an error with the image.') . $this->file('image')->getErrorMessage());
            }
        });
    }

    /**
     * Failed validation disable redirect
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax()) {
            throw new HttpResponseException(response()->json($validator->errors(), 200));
        }
    }
}
